package comvg.example.cursosandroid.micalculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText txtNum1;
    private EditText txtNum2;
    private EditText txtResul;
    private Button btnSumar, btnRestar, btnMulti, btnDiv, btnLimpiar, btnCerrar;
    private Operaciones op = new Operaciones();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initComponts();
    }
    public void initComponts(){
        txtNum1 = (EditText) findViewById(R.id.txtNum1);
        txtNum2 = (EditText) findViewById(R.id.txtNum2);
        txtResul = (EditText) findViewById(R.id.txtResul);

        btnSumar = (Button) findViewById(R.id.btnSuma);
        btnRestar = (Button) findViewById(R.id.btnResta);
        btnMulti = (Button) findViewById(R.id.btnMult);
        btnDiv = (Button) findViewById(R.id.btnDivi);

        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnCerrar = (Button) findViewById(R.id.btnCerrar);

        setEventos();
    }
    public void setEventos(){
        this.btnSumar.setOnClickListener(this);
        this.btnRestar.setOnClickListener(this);
        this.btnMulti.setOnClickListener(this);
        this.btnDiv.setOnClickListener(this);
        this.btnLimpiar.setOnClickListener(this);
        this.btnCerrar.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.btnSuma: //Sumar

                if (txtNum1.getText().toString().matches("") || txtNum2.getText().toString().matches("")) {

                    Toast.makeText(MainActivity.this, "Faltó captura un número", Toast.LENGTH_SHORT).show();
                }

                else {
                    op.setNum1(Float.valueOf(txtNum1.getText().toString()));
                    op.setNum2(Float.valueOf(txtNum2.getText().toString()));

                    txtResul.setText("" + op.suma());
                }
                break;

            case R.id.btnResta: //Restar

                if (txtNum1.getText().toString().matches("") || txtNum2.getText().toString().matches("")) {

                    Toast.makeText(MainActivity.this, "Faltó captura un número", Toast.LENGTH_SHORT).show();
                }

                else {
                    op.setNum1(Float.valueOf(txtNum1.getText().toString()));
                    op.setNum2(Float.valueOf(txtNum2.getText().toString()));
                    txtResul.setText("" + op.resta());
                }
                break;

            case R.id.btnMult:

                if (txtNum1.getText().toString().matches("") || txtNum2.getText().toString().matches("")) {

                    Toast.makeText(MainActivity.this, "Faltó captura un número", Toast.LENGTH_SHORT).show();
                }

                else {
                    op.setNum1(Float.valueOf(txtNum1.getText().toString()));
                    op.setNum2(Float.valueOf(txtNum2.getText().toString()));
                    txtResul.setText("" + op.mult());
                }
                break;

            case R.id.btnDivi:

                if (txtNum1.getText().toString().matches("") || txtNum2.getText().toString().matches("")) {

                    Toast.makeText(MainActivity.this, "Faltó captura un número", Toast.LENGTH_SHORT).show();
                }

                else {
                    op.setNum1(Float.valueOf(txtNum1.getText().toString()));
                    op.setNum2(Float.valueOf(txtNum2.getText().toString()));
                    txtResul.setText("" + op.div());
                }
                break;

            case R.id.btnLimpiar:
                txtNum1.setText("");
                txtNum2.setText("");
                txtResul.setText("");
                break;

            case R.id.btnCerrar:
                finish();
                break;
        }
    }
}
